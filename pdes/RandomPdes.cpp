#include "MUQ/Pde/PDE.h"

#include "gtest/gtest.h"

#include "MUQ/Utilities/HDF5Wrapper.h"
#include "MUQ/Utilities/EigenTestUtils.h"

#include "MUQ/Pde/PDEModPiece.h"
#include "MUQ/Pde/MeshParameter.h"
#include "MUQ/Inference/ProblemClasses/SamplingProblem.h"
#include "MUQ/Modelling/ModGraphPiece.h"
#include "MUQ/Pde/ElementInformation.h"
#include "MUQ/Inference/MCMC/MCMCState.h"
#include "MUQ/Inference/MCMC/DRKernel.h"

#include "MUQ/Pde/PointSensor.h"
#include "MUQ/Pde/Nontransient.h"
#include "MUQ/Modelling/VectorPassthroughModel.h"
#include "MUQ/Geostatistics/PowerKernel.h"
#include "MUQ/Pde/MeshKernelModel.h"
#include "MUQ/Utilities/RandomGenerator.h"

using namespace std;
using namespace muq::Utilities;
using namespace muq::Pde;
using namespace muq::Modelling;
using namespace muq::Inference;
using namespace Eigen;
namespace py = boost::python;


class PoissonExample : public Nontransient {
public:

PoissonExample(Eigen::VectorXi const& inputSizes, shared_ptr<GenericEquationSystems> const& system, boost::property_tree::ptree const& para) :
  Nontransient(inputSizes, system, para) {}

private:

  template<typename returnScalar, typename unknownScalar, typename parameterScalar>
  inline returnScalar ResidualImpl(string const& varName, libMesh::Real const phi, libMesh::RealGradient const& dphi, vector<unknownScalar> const& pressure, vector<vector<unknownScalar> > const& grad) const
  {
    // get the parameter number form the system
    const unsigned int var = vars.right.at(varName);
    
    
    const parameterScalar kappa = exp(GetScalarParameter<parameterScalar>("kappa"));
    const parameterScalar force    = GetScalarParameter<parameterScalar>("forcing");
    
    
    returnScalar resid = 0.0;
    
//         cout << "kappa " << kappa << " f " << force << endl;


    for (unsigned int i = 0; i < grad[0].size(); ++i) {
      resid += grad[0][i] * dphi(i);
    }
    
//     cout << "resid " <<  kappa * resid - phi * force << endl;

 //   return pressure[0] * kappa * resid - phi * force;
//  cout << "elem " << kappa * resid  << " " << phi * force << endl;
return kappa * resid - phi * force;
    
    
  }

  RESIDUAL(ResidualImpl)

  inline virtual bool IsDirichlet(libMesh::Point const& point) const override 
  { 
    if(point(0) == 0 || point(0) == 1){
    return true;
    }
//    if(point(1) == 0 || point(1) == 1){
 //   return true;
  //  }
    
    return false;
  }

  inline virtual double DirichletBCs(string const&, libMesh::Point const& pt) const override
  {
    return pow(pt(0), 2.0)*2;
  }

//   template<typename returnScalar, typename unknownScalar, typename parameterScalar>
//   inline vector<returnScalar> NeumannBCs(string const& varName, libMesh::Point const& pt, vector<unknownScalar> const& soln, vector<vector<unknownScalar> > const& grad) const
//   {
//     vector<returnScalar> neumman(3, 0.0);
// 
//     neumman[0] = 2.0 * pt(0);
//     neumman[1] = 2.0 * pt(1);
// 
//     return neumman;
//   }
// 
//   NEUMANN(NeumannBCs)
};

REGISTER_PDE(PoissonExample)



/// A (spatially dependent) forcing parameter
class SpatialForcingExample : public MeshParameter {
public:

  SpatialForcingExample(string const                            & paraName,
                        shared_ptr<GenericEquationSystems> const& parasystem,
                        boost::property_tree::ptree const       & para) :
    MeshParameter(paraName, parasystem, para) {}
    
    virtual ~SpatialForcingExample() = default;
private:

  /// Evaluate the mesh parameter
  virtual inline libMesh::Number ParameterEvaluate(double const x, double const y, double const,
                                                   string const&) const override
  {
    return (4.0*x + y);
  }
};

REGISTER_MESH_PARAMETER(SpatialForcingExample)



/// A (spatially dependent) forcing parameter
class DiffusionParam : public MeshParameter {
public:

  DiffusionParam(string const                            & paraName,
                        shared_ptr<GenericEquationSystems> const& parasystem,
                        boost::property_tree::ptree const       & para) :
    MeshParameter(paraName, parasystem, para) {}

    virtual ~DiffusionParam() = default;
private:

  /// Evaluate the mesh parameter
  virtual inline libMesh::Number ParameterEvaluate(double const x, double const y, double const,
                                                   string const&) const override
  {
    return 4*sin(2*3*x);
  }
};


REGISTER_MESH_PARAMETER(DiffusionParam)







/// A (spatially dependent) forcing parameter
class PiecewiseKappa : public MeshParameter {
public:

  PiecewiseKappa(string const                            & paraName,
                        shared_ptr<GenericEquationSystems> const& parasystem,
                        boost::property_tree::ptree const       & para) :
    MeshParameter(paraName, parasystem, para) {}

    virtual ~PiecewiseKappa() = default;
private:

  /// Evaluate the mesh parameter
  virtual inline libMesh::Number ParameterEvaluate(double const x, double const y, double const,
                                                   string const&) const override
  {
      Eigen::VectorXd parameters = system->GetEquationSystemsPtr()->parameters.get<Eigen::VectorXd>("coeff");
int numBlocks = 9;

VectorXd kappas(numBlocks+1);
kappas << 1.0, parameters;

int index = x*(numBlocks+1);
if(index == numBlocks+1){
  return kappas(numBlocks);
}
else{
return kappas(index);
}
  }
};


REGISTER_MESH_PARAMETER(PiecewiseKappa)




shared_ptr<ModPiece> MakeEllipticProblemInv(double noiseScale, string order, int gridSize, int obsGridSize,  bool includeBoundary)
{
  
  
  
    auto graph = make_shared<ModGraph>();

  boost::property_tree::ptree param;

  
    param.put("mesh.type", "LineMesh");
  param.put("mesh.N", gridSize);
  param.put("mesh.L", 1.0);
  param.put("mesh.B", 0.0);
  
  
  auto system = make_shared<GenericEquationSystems>(param);

    param.put("PDE.Unknowns", "u");
    param.put("PDE.UnknownOrders", order);
    param.put("PDE.GaussianQuadratureOrder", 20);
    param.put("PDE.Name", "PoissonExample");
    param.put("PDE.Parameters", "PiecewiseKappa,SpatialForcingExample");
    param.put("PDE.IsScalarParameter", "false,false");

    param.put("PDE.NoiseScale", 1.0/gridSize*noiseScale);

    param.put("KINSOL.LinearSolver", "Dense");



      
    param.put("MeshParameter.SystemName", "SpatialForcingExample");
    param.put("MeshParameter.VariableNames", "forcing");
    param.put("MeshParameter.VariableOrders", "1");
//     # param.put("MeshParameter.InputNames", "pumpingRates");
//     # param.put("MeshParameter.InputSizes", "4");

      auto pressureForceMod = MeshParameter::Create(system, param);

      
      unsigned paramDim = 9;

          param.put("MeshParameter.SystemName", "PiecewiseKappa");
    param.put("MeshParameter.VariableNames", "kappa");
    param.put("MeshParameter.VariableOrders", "1");
    param.put("MeshParameter.InputNames", "coeff");
    param.put("MeshParameter.InputSizes", "9");

      auto kappaMod = MeshParameter::Create(system, param);
      
//     param.put("MeshKernelModel.Name", "kappa");
//     param.put("MeshKernelModel.KLModes", paramDim);
//     param.put("MeshKernelModel.Order", 1);

//     double L = 0.1;
// double    power = 2.0;
//     double var = 25.0;

//     auto kernel = make_shared<muq::Geostatistics::PowerKernel>(L, power, var);
//     auto kappaMod = MeshKernelModel::Construct(system, kernel, param);
    
  Eigen::VectorXi inputSizes(2);
  inputSizes << kappaMod->outputSize, pressureForceMod->outputSize;
  
   std::vector<Eigen::VectorXd> obsPoints;
   
   
   if(!includeBoundary){
   obsPoints.resize(obsGridSize-1);
    for(unsigned i=1; i<=obsGridSize-1; ++i)
    {
      VectorXd point(1);
      point <<(i+0.)/static_cast<double>(obsGridSize);
      
      obsPoints.at(i-1) = point;
    }
   }
   else{
   obsPoints.resize(obsGridSize+1);
    for(unsigned i=0; i<=obsGridSize; ++i)
    {
      VectorXd point(1);
      point <<(i+0.)/static_cast<double>(obsGridSize);
//       cout << "point " << i << " " << point << endl;
      obsPoints.at(i) = point;
    }
   }

//     cout << "size " << pressureMod->outputSize << endl;

// cout << "making pde " << obsPoints.size() << endl;
    

      auto pressureMod = ConstructPDE(inputSizes,obsPoints, system, param);

// cout << "making pde ...worked" <<endl;

     
    
//     auto sensorMod = make_shared<PointSensor>(pressureMod->outputSize, obsPoints, "PoissonExample", system);


 
 
      
          graph->AddNode(make_shared<VectorPassthroughModel>(paramDim), "coeff");

	  
    graph->AddNode(kappaMod, "kappa");
graph->AddEdge("coeff", "kappa",0);

// 

// 
//     # the forcing inputs
    graph->AddNode(pressureForceMod, "Pressure Forcing");

//     # initial guess for solver
    graph->AddNode(make_shared<VectorPassthroughModel>(pressureMod->inputSizes(0)), "initial guess pressure");

//     # add the PDE to the graph;
    graph->AddNode(pressureMod, "Pressure");

// #    graph->writeGraphViz("ModGraph_nodes.pdf")

//     # connect the PDE's inputs
    graph->AddEdge("initial guess pressure", "Pressure", 0);

    graph->AddEdge("kappa", "Pressure", 1);
    graph->BindNode("initial guess pressure", VectorXd::Constant(pressureMod->inputSizes(0), 1.0));
    graph->AddEdge("Pressure Forcing", "Pressure", 2);
//      graph->writeGraphViz("elliptic1d_invA.pdf");


    auto forcingObs = make_shared<PointSensor>(pressureForceMod->outputSize, obsPoints, "SpatialForcingExample", system);
    graph->AddNode(forcingObs, "forcingObs");
    graph->AddEdge("Pressure Forcing", "forcingObs",0);

//     auto kappaObs = make_shared<PointSensor>(kappaMod->outputSize, obsPoints, "kappa", system);

//     graph->AddNode(kappaObs, "kappaObs");
//     graph->AddEdge("kappa", "kappaObs",0);


//     graph->AddNode(sensorMod, "PressureObs");
//     graph->AddEdge("Pressure", "PressureObs",0);
//      graph->writeGraphViz("elliptic1d_inv.pdf");
    

return ModGraphPiece::Create(graph, "Pressure");

  
}



class Elliptic1DForwardScaleProblem : public muq::Inference::AbstractSamplingProblem{
public:
  
	Elliptic1DForwardScaleProblem(int gridSize):AbstractSamplingProblem(1, nullptr, nullptr),gridSize(gridSize)	{
	  
	  
	    
	  
	  
			numRuns = 30;
	}
	virtual ~Elliptic1DForwardScaleProblem() = default;
	
	
	  virtual void                                    InvalidateCaches() override{};
	  
	  
  virtual std::shared_ptr<MCMCState> ConstructState(Eigen::VectorXd const& state, int const currIteration = 0, std::shared_ptr<muq::Utilities::HDF5LogEntry> logEntry = nullptr) override {
// 	cout << "constructing " << state.transpose() << endl;
	  double logPrior = 0;
	  double logLikelihood =0;


double noiseScale = pow(10.0,state(0));

	  MatrixXd forwardModelsBetter(gridSize-1, numRuns);
	  
	  unsigned paramDim = 9;

  unsigned numParam = 10;
	  for(unsigned j=0; j<numParam; ++j){

	    
	    VectorXd coeff = RandomGenerator::GetNormalRandomVector(paramDim);
// 	    cout << "coeff " << coeff.transpose() << endl;

//param set for siam talk:	    
// 	coeff << -1.5061233220e+0 ,  5.9597501480e-1,
//  3.8086849630e-1 , -1.1786223030e-1,
// -1.5861077760e+0 , -7.3832200240e-2,
//  3.1773408260e-1 ,  8.5348964130e-2,
//  1.6287352650e+0;
	    
	    
// 	    	coeff << 1.015061233220 ,  1.059597501480,
//  1.038086849630 , 1.011786223030,
// 1.015861077760 , 1.073832200240,
//  1.031773408260 , 1.085348964130,
//  1.016287352650;
 
//  	coeff << 1,1.0001,1,1,1,1,1.0001,1,1;
 
 
	    betterReference =MakeEllipticProblemInv(0.0, "1",  gridSize,  gridSize, false)->Evaluate(coeff);
	    worseReference = MakeEllipticProblemInv(0.0, "2",  gridSize,  gridSize, false)->Evaluate(coeff);
	    
// 	    cout << "betterReference " << betterReference.transpose() << endl;
// 	    cout << "worseReference " << worseReference.transpose() << endl;
	    
	  
	  for(unsigned i=0; i<numRuns; ++i){
	    auto model = MakeEllipticProblemInv(noiseScale, "1",  gridSize,  gridSize, false);


		   forwardModelsBetter.col(i)= model->Evaluate(coeff);	  
// cout << "sample run" << forwardModelsBetter.col(i).transpose() << endl;
	  }
		
		
// 		//single variate battacharyya
	  VectorXd betterMu = forwardModelsBetter.rowwise().mean();
	  VectorXd betterSigma = (forwardModelsBetter.colwise() - betterMu).rowwise().squaredNorm()/(numRuns-1.0);

		  VectorXd refSig = (betterReference - worseReference).array().square();
		  
		  
// 		  cout << " betterMu-betterReference" << betterMu-betterReference << endl;
// 		  cout << " betterMu" <<betterMu  << endl;
// 		  cout << " betterSigma" << betterSigma << endl;
// 		  cout << " refSig" <<refSig  << endl;
		   		   VectorXd bats = .25*log(.25*(refSig.array()/betterSigma.array() + betterSigma.array()/refSig.array() +2.0)) + .25*(betterMu-betterReference).array().square()/(refSig + betterSigma).array();
// 		  cout << " bats" <<bats  << endl;

logLikelihood -= bats.array().sum();

	  }
	  
	  logLikelihood /= static_cast<double>(numParam);

// 		  cout << " logLikelihood" <<logLikelihood  << endl;


if( std::isfinite( logLikelihood) && std::isfinite( logPrior)){
	  
	
	  return make_shared<muq::Inference::MCMCState>(state, logLikelihood,
            boost::optional<Eigen::VectorXd>(),
            boost::optional<double>(logPrior),
            boost::optional<Eigen::VectorXd>(),
            boost::optional<Eigen::MatrixXd>(),
//             boost::optional<Eigen::VectorXd>(VectorXd::Constant(1, batdist)));
		  boost::optional<Eigen::VectorXd>());
}
else
{
	cout << "bad constructed state, return null: "<< state.transpose() << endl;
	return nullptr;
	
}
	  
  };

  int numRuns;
  unsigned gridSize = 15;
  VectorXd betterReference;
	  
	  VectorXd worseReference;
};






class PseudomarginalMHKernel : public DR{
public:
	PseudomarginalMHKernel(std::shared_ptr<AbstractSamplingProblem> inferenceProblem, boost::property_tree::ptree& properties):
	DR(inferenceProblem, properties){
// 		cout << "Made pseudo kernel!" << endl;
	};
	
	virtual ~PseudomarginalMHKernel() = default;
	
	
// virtual double ComputeAcceptanceProbability(std::shared_ptr<muq::Inference::MCMCState> currentState,
//                                               std::shared_ptr<muq::Inference::MCMCState> proposedState) override
// {
// // 	cout << "old fwd" << currentState->GetForwardModel().transpose() << endl;
// 	auto revisedCurrent = samplingProblem->ConstructState(currentState->state);
// // 	cout << "updated fwd" << revisedCurrent->GetForwardModel().transpose() << endl;
// 	currentState->CloneFrom(revisedCurrent);
// // 	cout << "new fwd" << currentState->GetForwardModel().transpose() << endl;
// return MHKernel::ComputeAcceptanceProbability(currentState, proposedState);
// }

  virtual std::shared_ptr<muq::Inference::MCMCState> ConstructNextState(
    std::shared_ptr<muq::Inference::MCMCState>    currentState,
    int const                                     iteration,
    std::shared_ptr<muq::Utilities::HDF5LogEntry> logEntry) override{
		auto revisedCurrent = samplingProblem->ConstructState(currentState->state, iteration, logEntry);
if(revisedCurrent){
	currentState->CloneFrom(revisedCurrent);}
	else{
		cout << "current state revised bad, forcing a move" << endl;
		//our revision was bad, so force it to step away
		auto revised2 =  make_shared<muq::Inference::MCMCState>(currentState->state, 1e-30,
            boost::optional<Eigen::VectorXd>(),
            boost::optional<double>(-std::numeric_limits<double>::infinity()),
            boost::optional<Eigen::VectorXd>(),
            boost::optional<Eigen::MatrixXd>(),
            boost::optional<Eigen::VectorXd>());
		
		currentState->CloneFrom(revised2);
		
		
		
	}
// 	cout << "density " << currentState->GetDensity() << endl;
	 assert(!std::isnan( currentState->GetDensity())) ;
    
	return DR::ConstructNextState(currentState, iteration, logEntry);
	}	
};

REGISTER_MCMCKERNEL(PseudomarginalMHKernel);








class Elliptic1DInvProblem : public muq::Inference::AbstractSamplingProblem{
public:
	Elliptic1DInvProblem(py::list priorMean, py::list priorVar, py::list data, double obsVar, unsigned numRuns, unsigned gridSize, unsigned obsGridSize, double solverNoise):AbstractSamplingProblem(9, nullptr, nullptr),
	numRuns(numRuns), solverNoise(solverNoise), gridSize(gridSize),obsGridSize(obsGridSize)
	{
				VectorXd priorMeanVec = GetEigenVector<VectorXd>(priorMean);

		VectorXd priorVarVec = GetEigenVector<VectorXd>(priorVar);
		prior = make_shared<GaussianDensity>(priorMeanVec, priorVarVec, GaussianSpecification::DiagonalCovariance);
		VectorXd dataVec = GetEigenVector<VectorXd>(data);
		likelihood = make_shared<GaussianDensity>(dataVec, obsVar);
		dataStd = sqrt(obsVar);
		dataSize = dataVec.rows();
				
	}
	virtual ~Elliptic1DInvProblem() = default;
	
	
	
	  virtual void                                    InvalidateCaches() override{};
	  
	  
  virtual std::shared_ptr<MCMCState> ConstructState(Eigen::VectorXd const& state, int const currIteration = 0, std::shared_ptr<muq::Utilities::HDF5LogEntry> logEntry = nullptr) override {
// 	cout << "constructing " << state.transpose() << endl;
	  double logPrior = prior->LogDensity(state);
	  double logLikelihood =0;
	  
	  
	  
VectorXd forwardModelData(dataSize);

if( solverNoise == 0.0){
  auto fwdmodel = MakeEllipticProblemInv(solverNoise, "1", gridSize, obsGridSize,  false);
	    
forwardModelData = fwdmodel->Evaluate(state);


		  logLikelihood += likelihood->LogDensity(forwardModelData);
}
else{
	  for(unsigned i=0; i<numRuns; ++i){

	    auto fwdmodel = MakeEllipticProblemInv(solverNoise, "1", gridSize, obsGridSize,  false);
	    
forwardModelData = fwdmodel->Evaluate(state);


		  logLikelihood += likelihood->LogDensity(forwardModelData);

	  }
	  
}

	    logLikelihood /= static_cast<double>(numRuns);
// 	  	cout << "logLikelihood " << logLikelihood<< endl;


if( std::isfinite( logLikelihood) && std::isfinite( logPrior)){
	  
	
	  return make_shared<muq::Inference::MCMCState>(state, logLikelihood,
            boost::optional<Eigen::VectorXd>(),
            boost::optional<double>(logPrior),
            boost::optional<Eigen::VectorXd>(),
            boost::optional<Eigen::MatrixXd>(),
//             boost::optional<Eigen::VectorXd>(VectorXd::Constant(1, batdist)));
		  boost::optional<Eigen::VectorXd>());
}
else
{
	cout << "bad constructed state, return null: "<< state.transpose() << endl;
	return nullptr;
	
}
	  
  };

  int numRuns;
  double dataStd;
  int dataSize;
  shared_ptr<GaussianDensity> prior;
  shared_ptr<GaussianDensity> likelihood;	
  
  double solverNoise;
  unsigned gridSize;
  unsigned obsGridSize;
};







BOOST_PYTHON_MODULE(libRandomPdes)
{
  boost::python::def("MakeEllipticProblem",  MakeEllipticProblem);
    boost::python::def("MakeEllipticProblemInv",  MakeEllipticProblemInv);


  
     boost::python::class_<Elliptic1DForwardScaleProblem, std::shared_ptr<Elliptic1DForwardScaleProblem>,
                        boost::noncopyable> exportElliptic1DForwardScaleProblem(
    "Elliptic1DForwardScaleProblem",
    boost::python::init<int >());
  boost::python::implicitly_convertible<std::shared_ptr<Elliptic1DForwardScaleProblem>, std::shared_ptr<AbstractSamplingProblem> >();

  
  
  
    boost::python::class_<Elliptic1DInvProblem, std::shared_ptr<Elliptic1DInvProblem>,
                       boost::noncopyable> exportElliptic1DInvProblem(
    "Elliptic1DInvProblem",
    boost::python::init<py::list , py::list , py::list , double , unsigned , double , double , double >());

  boost::python::implicitly_convertible<std::shared_ptr<Elliptic1DInvProblem>, std::shared_ptr<AbstractSamplingProblem> >();
  
}