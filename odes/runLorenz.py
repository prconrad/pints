import sys

sys.path.append('/Users/patrick/muq_install/lib')
sys.path.append('/Users/patrick/Dropbox/Development/examples/build')
sys.path.append('/Users/patrick/Dropbox/Development/examples/odes/build')
sys.path.append('/Users/patrick/Dropbox/Development/examples')

# sys.path.append('/home/prconrad/local/lib')
# sys.path.append('/home/prconrad/local/muq_external/lib')
# sys.path.append('/media/extradata/Dropbox/Development/examples/odes/build')

import libmuqUtilities
import libmuqModelling
import libmuqInference
import libmuqApproximation
import libmuqPde
import libRandomOdes
import numpy as np
import matplotlib.pyplot as plt

#import plotTools

from mpi4py import MPI






def RunOneLorenz(runIndex,mcmcLength,stepInt, randomSolver):
    runName = 'lorenz_'



    if randomSolver:
        runName += "randomSolver_"
    else:
        runName += "deterministicSolver_"

    runName += str(stepInt)+'_'

    runName  += str(runIndex)

    print "Computing key ", runName

    params = dict()
    
    finalTime = 10.0
    
  
    if stepInt == 1:
        stepSize = .05
    elif stepInt == 2:
        stepSize = .025
    elif stepInt == 3:
        stepSize = .01
    else:
        print "no step size found, exiting!"
        sys.exit();

        
    # create MCMC instance
    params["MCMC.Steps"]            = mcmcLength-1

    params["MCMC.Method"]           = "SingleChainMCMC"
    params["MCMC.Kernel"] = "PseudomarginalMHKernel"
    params["MCMC.Proposal"] = "AM"

    params["MCMC.BurnIn"]           = 1
    params["MCMC.MHProposal.PropSize"]      = 1e-2
    params["MCMC.DR.stages"]     = 3
    params["MCMC.DR.scale"]     = 10.0
    
    params["MCMC.AM.AdaptSteps"] = 50
    params["MCMC.AM.AdaptStart"] = 200
    params["Verbose"]               = 5

    params["MCMC.HDF5OutputDetails"] = 1
    params["MCMC.HDF5OutputGroup"] = "/"+runName
    params["HDF5.OutputGroup"] = "/"+runName


    #else, continue by starting up MUQ
    # start logging
#    libmuqUtilities.InitializeLogging(["geneticsPython"])
    # open an hdf5


    startPoint = [-11, -5, 38,1]
    
    priorMean = startPoint
    priorVar = [1,1,1,1]

#    stateSize = 4
#    paramDim = stateSize






    finishTime = finalTime

    #numSteps = finishTime/stepSize+1
#    times = np.arange(0, finishTime+1E-14, stepSize).transpose()

    #numRuns = 100;
    obsInterval = .20
#    numObsTimes = int(finalTime/obsInterval)
#    numObs = numObsTimes*stateSize
    #obsTimes = np.arange(obsInterval, finishTime+1E-14, obsInterval).transpose()


    data = np.loadtxt('lorenzDataRk4_fine.dat')
    
#    print np.reshape(allData, [stateSize, 40],'F')
#    data = np.reshape(np.reshape(allData, [stateSize, 40],'F')[:,0:numObsTimes], [numObs,1],'F').transpose().tolist()[0]
    dataCov = 4

    lorenzSampling = libRandomOdes.LorenzSamplingProblem(priorMean, priorVar, data.tolist(), dataCov, 30, stepSize, obsInterval, finishTime)



   

    mcmc = libmuqInference.MCMCBase(lorenzSampling, params)
    mcmc.Sample(startPoint)


    #fig = plotTools.MakeFigure(1200,.7, True)


    # for j in range(numRuns):
    #     onePath = solver.Evaluate(startPoint)
    #
    #     pathMatrix = np.reshape(np.array(onePath), [stateSize, numSteps],'F')
    #
    #     for i in range(stateSize):
    #         plt.subplot(stateSize, 1, i+1)
    #         plt.plot(times, pathMatrix[i,:],plotTools.PlotColor('blue'),alpha=.2)
    #
    #
    # onePath = detsolver.Evaluate(startPoint)
    #
    # pathMatrix = np.reshape(np.array(onePath), [stateSize, finishTime/obsInterval],'F')
    #
    # for i in range(stateSize):
    #     plt.subplot(stateSize, 1, i+1)
    #     plt.plot(obsTimes, pathMatrix[i,:],'o',color=plotTools.PlotColor('red'),alpha=.6)
    # plt.show()



    #detsolver = libRandomOdes.EulerOdeSolver(lorenzRhs, stepSize, finishTime, obsInterval)
    #trueData = detsolver.Evaluate([startPoint])
    #print trueData



def RunOneLorenzForward(runIndex,mcmcLength,stepInt):
    runName = 'Lorenz_'



    runName += "forwardScale_"
    runName += str(stepInt)+'_'

    runName  += str(runIndex)

    print "Computing key ", runName

    params = dict()

    finalTime = 10.0


    if stepInt == 1:
        stepSize = .1
    elif stepInt == 2:
        stepSize = .05
    elif stepInt == 3:
        stepSize = .02
    elif stepInt == 4:
        stepSize = .01
    elif stepInt == 5:
        stepSize = .005
    else:
        print "no step size found, exiting!"
        sys.exit();


    # create MCMC instance
    params["MCMC.Steps"]            = mcmcLength-1

    params["MCMC.Method"]           = "SingleChainMCMC"

    params["MCMC.Kernel"] = "PseudomarginalMHKernel"

    params["MCMC.Proposal"] = "AM"

    params["MCMC.BurnIn"]           = 1
    params["MCMC.MHProposal.PropSize"]      = 1e-2
    params["MCMC.DR.stages"]     = 3
    params["MCMC.DR.scale"]     = 10.0

    params["MCMC.AM.AdaptSteps"] = 50
    params["MCMC.AM.AdaptStart"] = 200
    params["Verbose"]               = 5

    params["MCMC.HDF5OutputDetails"] = 1
    params["MCMC.HDF5OutputGroup"] = "/"+runName
    params["HDF5.OutputGroup"] = "/"+runName


    #else, continue by starting up MUQ
    # start logging
#    libmuqUtilities.InitializeLogging(["geneticsPython"])
    # open an hdf5



    startPoint = [0]





    finishTime = finalTime

    obsInterval = .2

    LorenzSampling = libRandomOdes.LorenzForwardScaleProblem(1e-2, 30, stepSize, obsInterval, finishTime)



    mcmc = libmuqInference.MCMCBase(LorenzSampling, params)
    mcmc.Sample(startPoint)






#
#def RestOfPlottingCode() 
#    inferenceTarget = libmuqModelling.VectorPassthroughModel(paramDim)
#    if randomSolver:
#        forwardModPiece = libRandomOdes.RandomRk4(lorenzRhs, stepSize, finishTime, obsInterval, noiseScale)
#        # forwardModPiece = libRandomOdes.RandomOdeSolver(lorenzRhs, stepSize, finishTime, obsInterval, noiseScale)
#    else:
#        forwardModPiece = libRandomOdes.RandomRk4(lorenzRhs, stepSize, finishTime, obsInterval, 0)
#        # forwardModPiece = libRandomOdes.EulerOdeSolver(lorenzRhs, stepSize, finishTime, obsInterval)
#    forwardModPiece.SetName('forwardModel')
#    forwardModel =libmuqModelling.GraphCompose(forwardModPiece, [inferenceTarget])
#
#    print "time ", finishTime
#    print "data", data
#    print "sim ", forwardModPiece.Evaluate([startPoint])
#
#    likelhoodMod = libmuqModelling.mvNormDens(data, dataCov, True)
#    likelhoodMod.SetName('likelihood')
#    likelihood= libmuqModelling.GraphCompose(likelhoodMod, [forwardModel])
#
#
#    priorMod = libmuqModelling.mvNormDens(priorMean, priorCov, True)
#    priorMod.SetName('prior')
#    prior           = libmuqModelling.GraphCompose(priorMod, [inferenceTarget])
#
#    posteriorMod = libmuqModelling.DensityProduct(2)
#    posteriorMod.SetName("posterior")
#    posterior =  libmuqModelling.GraphCompose(posteriorMod, [prior, likelihood])
#    posteriorEval = posterior.ConstructModPiece("posterior")
#
#    print "center posterior ", posteriorEval.Evaluate([startPoint])
##    posterior.writeGraphViz('graph.pdf')
#    # create the inference problem
##    test = libmuqInference.InferenceProblem(posterior)
#
#    delta = 2
#    ranges = np.array([[startPoint[0]-delta, startPoint[0]+delta],[startPoint[1]-delta, startPoint[1]+delta],[startPoint[2]-delta, startPoint[2]+delta]])
#    plotGrid = 101
#
#    # code to benchmark, apparently they python overhead doesn't matter!
#    # import time
#    # start = time.time()
#    # foo=0.0
#    # for i in range(1000):
#    #     foo = foo+ posteriorEval.Evaluate([startPoint])[0]
#    # end = time.time()
#    # print "elapsed", end-start, foo/1000
#    #
#    # start = time.time()
#    #
#    # foo = libRandomOdes.AveragePosterior(posteriorEval, startPoint, 1000);
#    # end = time.time()
#    #
#    # print "elapsed2", end-start, foo
#    # sys.exit()
#
#
#    numRandomSamples = 200
#    print '1D'
#
#
#    all_xs = []
#    all_ys = []
#    all_zs = []
#
#    for i in range(3):
#        print i
#        x = np.linspace(ranges[i,0], ranges[i,1], (plotGrid-1)*10+1,endpoint=True)
#        # print x
#        z = np.empty_like(x)
#        for k in range(len(x)):
#            import copy
#            point=copy.deepcopy(startPoint)
#
#            point[i] = x[k]
#            if randomSolver:
#                zs = np.zeros(numRandomSamples)
#                for m in range(numRandomSamples):
#                    zs[m] = posteriorEval.Evaluate([point])[0]
#                z[k] = np.mean(zs)
#            else:
#                z[k] = posteriorEval.Evaluate([point])[0]
#
#        all_xs.insert(i, [None]*i)
#        all_xs[i].insert(i, x)
#        all_ys.insert(i, [None]*i)
#        all_ys[i].insert(i, None)
#        all_zs.insert(i, [None]*i)
#        all_zs[i].insert(i, z)
#
#    print '2D'
#    for i in range(3):
#        for j in range(i):
#            print i,j
#            x,y = np.mgrid[ranges[i,0]:ranges[i,1]+.0001:(2.*delta/(plotGrid-1)), ranges[j,0]:ranges[j,1]+.0001:(2.*delta/(plotGrid-1))]
#            # print "made ", x,y
#            # x,y = np.mgrid[np.linspace(ranges[i,0], ranges[i,1], (plotGrid-1)*10+1,endpoint=True),np.linspace(ranges[,0], ranges[i,1], (plotGrid-1)*10+1,endpoint=True)]
#            z = np.empty_like(x)
#            for k in range(plotGrid):
#                for l in range(plotGrid):
#
#                    import copy
#                    point=copy.deepcopy(startPoint)
#
#                    point[i] = x[k,l]
#                    point[j] = y[k,l]
#
#                    if randomSolver:
#                        zs = np.zeros(numRandomSamples)
#                        for m in range(numRandomSamples):
#                            zs[m] = posteriorEval.Evaluate([point])[0]
#                        z[k,l] = np.mean(zs)
#                    else:
#                        z[k,l] = posteriorEval.Evaluate([point])[0]
#            all_xs[i][j]=x
#            all_ys[i][j]=y
#            all_zs[i][j]=z
#
#    # print all_xs
#    # print all_ys
#    # print all_zs
#
#    exampleName = "lorenz"
#    if exampleName == 'rosenbrock':
#        label_separation_x = 0.1
#    else:
#        label_separation_x = 0.1
#    presentationVersion = True
#    if presentationVersion:
#        fig = plotTools.MakeFigure(1200,.7, presentationVersion)
#    else:
#        fig = plotTools.MakeFigure(425,.9, presentationVersion)
#
#    labels = ['x(0)','y(0)','z(0)']
#    import surface_matrix
#    surface_matrix.surface_matrix(all_xs,all_ys,all_zs, labels=labels, ax=plt.gca(), label_separation_x=label_separation_x, ranges=ranges)
#
##https://github.com/olgabot/prettyplotlib/wiki/scatter-and-pcolormesh:-Motivating-examples
#            #http://bl.ocks.org/mbostock/5577023
#
#    # from prettyplotlib import brewer2mpl
#    #
#    # blueColormap = brewer2mpl.get_map('PuBu', 'sequential', 9).mpl_colormap
#    # ax=plt.subplot2grid((3,3),(i,j))
#    #
#    #
#    # plt.pcolormesh(x,y,z,cmap=blueColormap)
#
#    plt.savefig('plots/'+runName + '.pdf', format='pdf')
#
#    plt.close()

    # plt.show()
#    for i in range(3):
#        for j in range(i):
#            plt.subplot2grid(3,3,(i-1)*3+j)
 #           x,y = np.mgrid(np.linspace(ranges[i,1], ranges[i,2], plotGrid), np.linspace(ranges[j,1], ranges[j,2], plotGrid))




#    mcmc = libmuqInference.MCMCBase(test, params)


#    mcmc.Sample(startPoint)

def generateData(perfectData):
    print "starting generate"

    stateSize = 3
    if not perfectData:
        stepSize = .0001
    else:
        stepSize = .05
    finishTime = 10
    finalTime = 10
    #noiseScale = 5
    #numSteps = finishTime/stepSize+1
    times = np.arange(0, finishTime+1E-14, stepSize).transpose()

    obsInterval = .2
    numObsTimes = int(finalTime/obsInterval)



    lorenzRhs = libRandomOdes.LorenzRhs()
    detsolver = libRandomOdes.RandomRk4(lorenzRhs, stepSize, finishTime, obsInterval, 0)



    startPoint = [-11, -5, 38]

    onePath = detsolver.Evaluate([startPoint])
    print "generate ", onePath

    if perfectData:
        print "perfect"
        np.savetxt('lorenzDataRk4.dat', onePath)
    else:
        print "not perfect"
        np.savetxt('lorenzDataRk4_fine.dat', onePath)

def plotData(perfectData):
    print "starting plot"

    stateSize = 3
    if not perfectData:
        stepSize = .0001
    else:
        stepSize = .05
    finishTime = 10
    finalTime = 10
    #noiseScale = 5
    #numSteps = finishTime/stepSize+1
    times = np.arange(0, finishTime+1E-14, stepSize).transpose()

    #numRuns = 100;
    obsInterval = .2
    numObsTimes = int(finalTime/obsInterval)
    numObs = numObsTimes*stateSize
    obsTimes = np.arange(obsInterval, finishTime+1E-14, obsInterval).transpose()
    stateSize = 3

    if perfectData:
        print "perfect"
        allData = np.loadtxt('lorenzDataRk4.dat')
    else:
        print "not perfect"
        allData = np.loadtxt('lorenzDataRk4_fine.dat')
#    print np.reshape(allData, [stateSize, 40],'F')
    data = np.reshape(allData, [stateSize, 5*10],'F')


    lorenzRhs = libRandomOdes.LorenzRhs()
    detsolver = libRandomOdes.RandomRk4(lorenzRhs, stepSize, finishTime, stepSize, 0)



    fig = plotTools.MakeFigure(800,.7, True)


    startPoint = [-11, -5, 38]

    onePath = detsolver.Evaluate([startPoint])
    print "regenerate ", onePath

    pathMatrix = np.reshape(np.array(onePath), [stateSize, finishTime/stepSize],'F')

    print "path", pathMatrix[0,:]
    print "data", data[0,:]
    print "path", pathMatrix[1,:]
    print "data", data[1,:]

    print "path", pathMatrix[2,:]

    print "data", data[2,:]
    print "shape1", pathMatrix.shape
    print "timeshpae", times.shape
    for i in range(stateSize):
        plt.subplot(stateSize, 1, i+1)
        plt.plot(times[1:], pathMatrix[i,:],'-',color=plotTools.PlotColor('blue'),alpha=1)
    for i in range(stateSize):
        plt.subplot(stateSize, 1, i+1)
        plt.plot(obsTimes, data[i,:],'o',color=plotTools.PlotColor('red'),alpha=.6)

    plt.subplot(3,1,1)
    plt.ylabel('x')
    plt.gca().xaxis.set_visible(False)
    plt.subplot(3,1,2)
    plt.ylabel('y')
    plt.gca().xaxis.set_visible(False)
    plt.subplot(3,1,3)
    plt.ylabel('z')
    plt.xlabel('t')

    # if perfectData:
    #     plt.savefig('plots/lorenz_'+'noiseFree_trajectory.pdf', format='pdf')
    # else:
    plt.savefig('plots/lorenz_'+'noisy_trajectory.pdf', format='pdf')


def plotRandomPaths(perfectData, stepSize, newfig, noise=pow(10.0,4)):
    import plotTools


    stateSize = 3
#    stepSize = .004
    finishTime = 10
    finalTime = 10
    #noiseScale = 5
    #numSteps = finishTime/stepSize+1
    times = np.arange(0, finishTime+1E-14, stepSize).transpose()

    #numRuns = 100;
    obsInterval = .2
    numObsTimes = int(finalTime/obsInterval)
    numObs = numObsTimes*stateSize
    obsTimes = np.arange(obsInterval, finishTime+1E-14, obsInterval).transpose()
    stateSize = 3

    if perfectData:
        print "perfect"
        allData = np.loadtxt('lorenzDataRk4.dat')
    else:
        print "not perfect"
        allData = np.loadtxt('lorenzDataRk4_fine.dat')
#    print np.reshape(allData, [stateSize, 40],'F')
    data = np.reshape(allData, [stateSize, finishTime/obsInterval],'F')


    lorenzRhs = libRandomOdes.LorenzRhs()
    detsolver = libRandomOdes.RandomRk4(lorenzRhs, stepSize, finishTime, stepSize, noise)    
#    detsolver = libRandomOdes.RandomRk2(lorenzRhs, stepSize, finishTime, stepSize, pow(10.0,2.6))    
#    detsolver = libRandomOdes.RandomOdeSolver(lorenzRhs, stepSize, finishTime, stepSize, pow(10.0,2))



    if newfig:
        fig = plotTools.MakeFigure(1400,.7, True)


    startPoint = [-11, -5, 38]

    for j in range(100):
        onePath = detsolver.Evaluate([startPoint])

        pathMatrix = np.reshape(np.array(onePath), [stateSize, finishTime/stepSize],'F')

        for i in range(stateSize):
            plt.subplot(stateSize, 1, i+1)
            if newfig:
                plt.plot(times[1:], pathMatrix[i,:],'-',color=plotTools.PlotColor('blue'),alpha=.1)                
            else:
                plt.plot(times[1:], pathMatrix[i,:],'-',color=plotTools.PlotColor('purple'),alpha=.1)
                
    for i in range(stateSize):
        plt.subplot(stateSize, 1, i+1)
        plt.plot(obsTimes, data[i,:],'o',color=plotTools.PlotColor('red'),alpha=.8)

    plt.subplot(3,1,1)
    plt.ylabel('x')
    plt.gca().xaxis.set_visible(False)
    plt.subplot(3,1,2)
    plt.ylabel('y')
    plt.gca().xaxis.set_visible(False)
    plt.subplot(3,1,3)
    plt.ylabel('z')
    plt.xlabel('t')




    # if perfectData:
    #     plt.savefig('plots/lorenz_'+'noiseFree_randomTrajectory.pdf', format='pdf')
    # else:
    #     plt.savefig('plots/lorenz_'+'noisy_randomTrajectory.pdf', format='pdf')


comm = MPI.COMM_WORLD

pdeinit = libmuqPde.LibMeshInit(sys.argv)

runLength = 10000
jobs = []

#generateData(False)
# generateData(False)
#
#plotData(False)
# plotData(False)
# plotRandomPaths(True)
#plotRandomPaths(False, .008)
#plotRandomPaths(False, .004)
#plotRandomPaths(False, .002)

#plotRandomPaths(False, .2, True)
#plotRandomPaths(False, .1, False)


# plotRandomPaths(False, .1, True,pow(10.0,3.1))
# plotRandomPaths(False, .05, False,pow(10.0,3.1))
# plt.savefig('plots/lorenz_'+'noisy_trajectory_05.pdf', format='pdf')

#
# plotRandomPaths(False, .05, True,pow(10.0,3.5))
# plotRandomPaths(False, .025, False,pow(10.0,3.5))
# plt.savefig('plots/lorenz_'+'noisy_trajectory_025.pdf', format='pdf')

##
# plotRandomPaths(False, .02, True,pow(10.0,4.4))
# plotRandomPaths(False, .01, False,pow(10.0,4.4))
# plt.savefig('plots/lorenz_'+'noisy_trajectory_01.pdf', format='pdf')

##
#plotRandomPaths(False, .01, True)
#plotRandomPaths(False, .005, False)
# plt.show()
# sys.exit()

#for i in range(10):
#for i in range(30):#[4]:
#    seed = libmuqUtilities.RandomGeneratorTemporarySetSeed(i+87478)
for i in range(1):

    # jobs.append(lambda i=i: RunOneLorenz(i,mcmcLength=runLength, finalTime = 2, perfectData=True, randomSolver=False))
    # jobs.append(lambda i=i: RunOneLorenz(i,mcmcLength=runLength, finalTime = 4, perfectData=True, randomSolver=False))
    # jobs.append(lambda i=i: RunOneLorenz(i,mcmcLength=runLength, finalTime = 6, perfectData=True, randomSolver=False))
    # jobs.append(lambda i=i: RunOneLorenz(i,mcmcLength=runLength, finalTime = 8, perfectData=True, randomSolver=False))
    # jobs.append(lambda i=i: RunOneLorenz(i,mcmcLength=runLength, finalTime = 10, perfectData=True, randomSolver=False))

#    jobs.append(lambda i=i: RunOneLorenz(i,mcmcLength=runLength, finalTime = 2, perfectData=False, randomSolver=False))
#    jobs.append(lambda i=i: RunOneLorenz(i,mcmcLength=runLength, finalTime = 4, perfectData=False, randomSolver=False))
#    jobs.append(lambda i=i: RunOneLorenz(i,mcmcLength=runLength, finalTime = 6, perfectData=False, randomSolver=False))
#    jobs.append(lambda i=i: RunOneLorenz(i,mcmcLength=runLength, finalTime = 8, perfectData=False, randomSolver=False))
#    jobs.append(lambda i=i: RunOneLorenz(i,mcmcLength=runLength, finalTime = 10, perfectData=False, randomSolver=False))


    # jobs.append(lambda i=i: RunOneLorenz(i,mcmcLength=runLength, finalTime = 2, perfectData=True, randomSolver=True))
    # jobs.append(lambda i=i: RunOneLorenz(i,mcmcLength=runLength, finalTime = 4, perfectData=True, randomSolver=True))
    # jobs.append(lambda i=i: RunOneLorenz(i,mcmcLength=runLength, finalTime = 6, perfectData=True, randomSolver=True))
    # jobs.append(lambda i=i: RunOneLorenz(i,mcmcLength=runLength, finalTime = 8, perfectData=True, randomSolver=True))
    # jobs.append(lambda i=i: RunOneLorenz(i,mcmcLength=runLength, finalTime = 10, perfectData=True, randomSolver=True))

#    jobs.append(lambda i=i: RunOneLorenz(i,mcmcLength=runLength, finalTime = 10, perfectData=False, randomSolver=True))
#    jobs.append(lambda i=i: RunOneLorenz(i,mcmcLength=runLength, finalTime = 8, perfectData=False, randomSolver=True))

#    jobs.append(lambda i=i: RunOneLorenz(i,mcmcLength=runLength, finalTime = 2, perfectData=False, randomSolver=True))
#    jobs.append(lambda i=i: RunOneLorenz(i,mcmcLength=runLength, finalTime = 4, perfectData=False, randomSolver=True))
#    jobs.append(lambda i=i: RunOneLorenz(i,mcmcLength=runLength, finalTime = 6, perfectData=False, randomSolver=True))

    # jobs.append(lambda i=i: RunOneLorenz(i,mcmcLength=runLength,stepInt=1, randomSolver=True))
    # jobs.append(lambda i=i: RunOneLorenz(i,mcmcLength=runLength,stepInt=2, randomSolver=True))
    # jobs.append(lambda i=i: RunOneLorenz(i,mcmcLength=runLength,stepInt=3, randomSolver=True))

    # jobs.append(lambda i=i: RunOneLorenzForward(i,mcmcLength=runLength,stepInt=1))
    # jobs.append(lambda i=i: RunOneLorenzForward(i,mcmcLength=runLength,stepInt=2))
    jobs.append(lambda i=i: RunOneLorenzForward(i,mcmcLength=runLength,stepInt=3))
    jobs.append(lambda i=i: RunOneLorenzForward(i,mcmcLength=runLength,stepInt=4))
    jobs.append(lambda i=i: RunOneLorenzForward(i,mcmcLength=runLength,stepInt=5))


#    jobs.append(lambda i=i: RunOneLorenz(i,mcmcLength=runLength,stepInt=4, randomSolver=True))
    

#seed = libmuqUtilities.RandomGeneratorTemporarySetSeed(87478)
#libmuqUtilities.HDF5Wrapper.OpenFile('results/genetics_regression_looser.h5_rank_'+ str(comm.Get_rank()))
libmuqUtilities.HDF5Wrapper.OpenFile('results/lorenz_fwd.h5_rank_'+ str(comm.Get_rank()))

for aJob in jobs[comm.Get_rank()::comm.Get_size()]:
    aJob()
comm.barrier()
libmuqUtilities.HDF5Wrapper.CloseFile()



